---
title: "Machine Learning con R - Sesión 2"
subtitle: "Programa Ejecutivo de Inteligencia Artificial y Deep Learning"
author: "<br>Antonio Sánchez Chinchón"
date: "2018/06/02"
output:
  xaringan::moon_reader:
    css: ["eoi", "eoi-fonts"]
    lib_dir: libs
    nature:
      highlightStyle: tomorrow-night-eighties
      highlightLanguage: javascript, r, python, bash
      highlightLines: true
      countIncrementalSlides: false
---

```{r setup, include=FALSE}
options(htmltools.dir.version = FALSE)
```

class: inverse, center, middle

# Métodos de remuestreo

---
# Métodos de remuestreo

+ Los métodos de remuestreo son una **herramienta indispensable en Machine Learning**. 

+ Consisten en extraer distintas muestras del conjunto de entrenamiento **con distintas finalidades**:

  + **Estimar la precisión** de algún estadístico o un modelo
  
  + **Seleccionar** algún parámetro de un modelo (ejemplo, la $K$ de KNN)
  
  + **Comparar** modelos entre sí
  

+ Los métodos de remuestreo permiten obtener información que **no tendríamos si estimaramos el modelo una única vez**.

+ Han tomado fuerza conforme a medida que los ordenadores han ganado potencia ya que son **computacionalmente exigentes**

---
# Métodos de remuestreo

Hay dos tipos fundamentales de métodos:

  + **Bootstrap**: Es una técnica **muy extendida** y **potente** que se utiliza principalmente para estimar la precisión de un estadístico asociado a algún método de machine learning

    + **Por ejemplo**, medir el error estandar de los coeficientes de una regresión lineal
  
  + **Validación cruzada (*cross-validation*)**: Su propósito es la **verificación de un modelo**, no su construcción<sup>*</sup>: sirve para medir la precisión con que un modelo puede ser entrenado por algunos datos para predecir otros que no ha visto
  
    + Verificando varios modelos podemos **compararlos** entre sí
  
  
.footnote[(*) Hay métodos inspirados en cross validation que generan a su vez modelos (model ensemble) y que veremos más adelante]
---
# Bootstraping

El siguiente ejemplo calcula mediante un **bootstraping** de 1000 repeticiones el intervalo de confianza al 95% para el R-cuadrado en la regresión lineal de `sales` respecto a `TV` con el dataset `Advertising`:

```{r, message=FALSE, warning=FALSE}
library(boot)
advertising <- read.csv("data/Advertising.csv")
attach(advertising)
# Funcion para obtener el R-Squared
rsq <- function(formula, data, indices) {
  d <- data[indices,] # permite a boot seleccionar la muestra
  fit <- lm(formula, data=d)
  return(summary(fit)$r.square)
} 
# bootstrapping con 1000 repeticiones 
results <- boot(data=advertising, 
                statistic=rsq, 
                R=1000, 
                formula=sales~TV)

```

---
# Bootstraping

```{r, message=FALSE, warning=FALSE}
results
```

---
# Bootstraping

```{r, fig.align='center', fig.height=4,  fig.width=7, dev='svg'}
plot(results)
```

.footnote[Nota: el gráfico de la derecha se llama Q-Q Plot (más información [aquí](http://data.library.virginia.edu/understanding-q-q-plots/))]
---
# Bootstraping

```{r, message=FALSE, warning=FALSE}
# Bootstrap 95% CI para el R-cuadrado
boot.ci(results, type="bca")

```
---
# Cross-validation

Tradicionalmente la estrategia para estimar y medir el error de un modelo era dividir el conjunto de datos en dos subconjuntos:

  + El de **entrenamiento** que se utiliza para ajustar los parámetros del modelo y que contiene la mayoria de la observaciones (típicamente enre un 75% y un 80%)
  
  + El de **validación o test**, que se emplea para medir la tasa de error, que contiene el resto de observaciones
  
Este enfoque tiene **dos inconvenientes importantes**:

  + El error, que puede tener una varianza alta, **se mide con una parte muy pequeña de los datos**
  
  + Los parámetros no se miden con todos los datos, lo que **reduce el rendimiento de los modelos**

---
# Cross-validation

La validación cruzada consiste en **repetir muchas veces el enfoque clásico del conjunto de entrenamiento/validación** con distintas observaciones en dichos conjuntos cada vez

Esto sirve para **seleccionar** el modelo más adecuado así como para realizar **estimaciones precisas de su grado de acierto**.

Hay varias maneras de dividir los datos en subconjuntos para hacer cross-validation; la más utilizada es la *k-Fold Cross Validation*:

  + Consiste en **dividir los datos en k subconjuntos** (*folds*) del mismo tamaño aproximadamente. 

  + El modelo entonces **se ajusta k veces**. 

  + La primera vez deja el **1er subconjunto como conjunto de validación y los k-1 restantes como entrenamiento**; la 2ª vez utiliza el 2º fold como test y los k-1 restantes como entrenamiento y así sucesivamente ...

  + El ECM estimado por k-folds es entonces la **media de los k errores cuadráticos medios estimados en cada iteración**
  
---
# Cross-validation

El siguiente ejemplo calcula mediante un **k-folds cross-validation** el valor óptimo de $K$ en un KNN para prever el valor `default` del dataset `Default`:

```{r, message=FALSE, warning=FALSE}
library(ISLR)
library(caret)
data(Default)
train_control <- trainControl(method="cv", number=10)
# Probamos K entre 3 y 15
grid = expand.grid(k = c(3:15))
# Cross-validation
model <- train(default~., 
               data=Default, 
               trControl=train_control, 
               method="knn", 
               tuneGrid=grid)

```

---
# Cross-validation

```{r, fig.align='center', fig.height=4,  fig.width=7, dev='svg'}
plot(model)
```


---
class: inverse, center, middle

# Análisis discriminante lineal (LDA)
---
# Análisis discriminante lineal (LDA)

+ El análisis discriminante (LDA), al igual que la regresión logística, es un **modelo de clasificación supervisado** que mide las probabilidades de pertenencia a cada clase $k$ de una observación $Pr(Y=k \vert X=x)$, que para simplificar vamos a llamar $p_k(X)$

+ Produce estimaciones **más robustas** que la regresión logística y se usa más que ésta cuando tenemos **más de dos clases**

+ **Ajusta la distribución condicionada de cada clase separado** y luego construye un modelo global apoyándose en el **Teorema de Bayes**<sup>1</sup>



.footnote[(1): Planteado por el matemático británico y ministro presbiteriano Thomas Bayes ¡en 1763!]

---
# Análisis discriminante lineal (LDA)

+ Supón que queremos clasificar una observación en una de $K$ clases, con $K \geq 2$, y que conocemos la probabilidad a priori $\pi_k$ de que una observación pertenezca a cada clase:

+ El **teorema de Bayes** dice que:

$\large p_k(X)=\dfrac{\pi_k Pr(X=x \vert Y=k)}{\sum_{l=1}^{K} \pi_l Pr(X=x \vert Y=l)}$

+ Para simplificar, llamaremos $f_k(x)=Pr(X=x \vert Y=k)$

+ Los valores de $\pi_k$ son *sencillos* de calcular si tenemos una muestra aleatoria de la población pero estimar $f_l(x)$ **es más complicado** a no ser que se asuma que esa distribución tiene alguna *forma* conocida

+ El LDA **calcula** estas distribuciones y luego **las junta con la fórmula de Bayes** para construir el clasificador

---
# Análisis discriminante lineal (LDA)

+ Las fórmulas de un LDA son un tanto complejas así que para que se entiendan mejor vamos a *dar unas pinceladas* para el caso de que **sólo tuviéramos un predictor** (sólo una $X$)

+ Queremos estimar $f_k(x)$ para *enchufarlo* en la fórmula de Bayes y poder calcular $p_k(x)$
  + Clasificaremos a $x$ en la clase $k$ que tenga el máximo $p_k(x)$
  
+ Supongamos que $f_k(x)$ sigue una **distribución normal** (*Gausiana*):

$f_k(x)=\dfrac{1}{\sqrt{2\pi}\sigma_k}exp(- \dfrac{1}{2\sigma^2_k}(x-\mu_k)^2)$

  en donde $\mu_k$ y $\sigma^2_k$ son la media y la varianza de la k-ésima clase

+ Supongamos también que $\sigma^2_1=\sigma^2_2=...=\sigma^2_K$

---
# Análisis discriminante lineal (LDA)

+ Sustituyendo el valor de $f_k(x)$ en la fórmula de Bayes tenemos que:

$p_k(x)=\dfrac{\dfrac{\pi_k}{\sqrt{2\pi}\sigma}exp(- \dfrac{1}{2\sigma^2}(x-\mu_k)^2)}{\sum_{l=1}^{K} \dfrac{\pi_l}{\sqrt{2\pi}\sigma}exp(- \dfrac{1}{2\sigma^2}(x-\mu_l)^2)}$

+ Tomando logaritmos, es sencillo demostrar que la expresión anterior es equivalente a clasificar a $x$ en la clase $k$ **para la que la siguiente expresión es máxima**:

$\delta_k(x)=x\cdot\dfrac{\mu_k}{\sigma^2}-\dfrac{\mu_k^2}{2\sigma^2}+log(\pi_k)$

+ En la práctica, los valores de $\pi_k$, $\mu_k$ y $\sigma^2$ **se aproximan por sus correspondientes estadísticos muestrales** a no ser que se conozca el valor poblacional de alguno de ellos, como los *a prioris* $\pi_k$

.footnote[Se llama lineal porque los clasificadores **son funciones lineales** de las x`s]

---
# Análisis discriminante lineal (LDA)

+ En el caso de $p>1$ predictores, el clasificador LDA asume que las **observaciones de la clase k-ésima provienen de una distribución normal multivariante y que la covarianza de las variables es común para todas las clases**

+ Al igual que antes, obtiene $K$ clasificadores y **asigna la observación a la clase cuyo valor del clasificador es máximo**

+ Antes de hacer un LDA es importante **quitar outliers**, **acercar a la normalidad la distribución de las variables** y **estandarizarlas** para que las varianzas sean iguales (a 1)

+ En R se puede hacer un LDA con la función `lda` del paquete `MASS`. Por ejemplo

```{r, eval=FALSE}
MASS::lda(Species ~ ., iris, prior = c(1,1,1)/3)
```

  hace un LDA sobre el dataset `ìris` con *a prioris* del `33%` para cada una de las 3 clases.
---

class: inverse, center, middle

# Regularización

---
# Regularización

En ocasiones los modelos de regresión estimados por mínimos cuadrados sufren de problemas de varianza:
+ Esto es muy palpable cuando tenemos muchas variables (p) y pocas observaciones (n)
+ De hecho, el método de mínimos cuadrados, la solución por mínimos cuadrados no es única (varianza *infinita*)

Otras veces **nos sobran variables** y nos gustaría tener modelos más *parsiomoniosos*, cuya interpretación fuera más fácil (y también más robustos)

Existen muchos métodos para hacer selección de variables (*subset selection*) y en esta sección vamos a estudiar dos de los más potentes: la **regresión ridge** y la **regresión lasso**

Ambos métodos forman parte de un conjunto de técnicas llamadas **regularización** (o *shrinkage*)


---
# Regularización Ridge

Los parámetros $\beta_0, \beta_1,...,\beta_p$ estimados por el método de mínimos cuadrados son **los que minimizan la siguiente expresión**:

$\large RSS=\sum_{i=1}^{n}(y_i-\beta_0-\sum_{j=1}^{p}\beta_jx_{ij})^2$

La regresión ridge es muy similar al método de mínimos cuadrados pero **la expresión que se minimiza es la siguiente**:

$\large \sum_{i=1}^{n}(y_i-\beta_0-\sum_{j=1}^{p}\beta_jx_{ij})^2+\lambda\sum_{j=1}^{p}\beta_j^2=RSS+\lambda\sum_{j=1}^{p}\beta_j^2$

En donde $\lambda \ge 0$ es un parámetro de *tunning* que se determina **por separado**

Seleccionar un buen valor de $\lambda$ es **crítico**: una buena manera de hacerlo es usar la **validación cruzada**

---
# Regularización Ridge

Intuitivamente, la regresión ridge **penaliza la inclusión de variables**, o dicho de otra manera, obliga a que la reducción del error generada por $X_i$ *compense* la penalización asociada al parámetro $\beta_i$

Por otro lado, ahora es muy importante **estandarizar la variables antes de estimar** el modelo: si no, la propia escala puede ser la que determine que una variable juegue no en el modelo final

Los predictores se estandarizan **de la siguiente manera**:

$\large \tilde x_{ij}=\dfrac{x_{ij}}{\sqrt{\frac{1}{n}\sum_{i=1}^{n}(x_{ij}-\bar x_j)^2}}$

---
# Regularización Ridge

Cuanto mayor es el valor de $\lambda$, la *flexibilidad* de la regresión ridge **decrece**, con lo que también **decrece la varianza pero aumenta el bias**
  + En muchos casos, un buen $\lambda$ suele producir un **decremento significativo del  RSS respecto a la regresión lineal**.

La regresión *ridge* tiene una gran **desventaja**, y es que sigue teniendo en cuenta a todos los predictores, y la penalización $\lambda \sum \beta_j^2$ hace que los parámetros se hagan pequeños **pero no siempre se anulen**:
  + Es decir: puede que sigan saliendo todas las variables, con los consecuentes problemas de interpretabilidad


---
# Regularización Lasso

La regresión lasso *arregla* el problema de la regresión ridge **minimizando la siguiente expresión**:

$\large \sum_{i=1}^{n}(y_i-\beta_0-\sum_{j=1}^{p}\beta_jx_{ij})^2+\lambda\sum_{j=1}^{p}\left |\beta_j\right |=RSS+\lambda\sum_{j=1}^{p}\left |\beta_j\right |$

Ahora, la penalización **fuerza a que algunos coeficientes estimados sean exactamente cero**, con lo que los modelos resultantes son **más fáciles de interpretar**
  + La regresión lasso hace **selección de variables**

---

# Regularización: interpretación geométrica

Interpretación geométrica de la regresión lasso (izqda.) y de la ridge (dcha.)

<img src="img/6.7.png" height="400" align="middle">

---

# Regularización: overview en R

+ Tanto la regresión lasso como la ridge necesitan **variables numéricas estandarizadas**:
  + Para convertir las variables cualitativas en variables numéricas *dummy*, se  utiliza la función `model.matrix`
  + Para estandarizar las variables se utiliza la función `scale`

+ Ambas dos se estiman con el paquete `glmnet`:
  + La función `glmnet` estima una ridge para el parámetro `alpha=0` y estima una lasso para el parámetro `alpha=1`
  + El mejor valor de $\lambda$ se estima mediante cross-validation con la función `cv.glmnet` en ambos casos (con el correspondiente valor anterior de `alpha`)

---

class: inverse, center, middle

# Métodos basados en árboles

---
# Métodos basados en árboles

+ Los árboles de decisión son modelos **supervisados** que pueden resolver problemas de **regresión** (si la $Y$ es cuantitativa) o de **clasificación** (si es cualitativa)

+ **Particionan** el espacio de definido por los predictores (las $X$ del modelo) **en regiones más simples**; para hacerlo **tienen en cuenta la variable dependiente** (la $Y$), de manera que los estratos resultantes tengan un valor parecido de ella
  
+ La predición se hace típicamente *asignando la media o la moda* de la $Y$ del estrato *en el que cae* la nueva observación
  
+ Son **útiles** y **fáciles de interpretar** (y por tanto de explicar) aunque su *performance* suele ser inferior a la de modelos paramétricos como la regresión lineal, los GAM o el análisis discriminante

+ No obstante, existen enfoques (*bagging*, *random forest* y *boosting*) que **estiman numerosos árboles** y los **combinan** para crear *predicciones de consenso* y que mejoran notablemente la precisión de los árboles de decisión *simples*

---
# Árboles de regresión

+ Veamos un ejemplo, utilizando el conjunto de datos `Hitters`

+ Este dataset contiene datos de 322 jugadores de la MLB (Major League Baseball) de las temporadas de 1986 a 1987.

+ En concreto vamos a predecir el **salario** (`Salary`) de un jugador en función del número de ** *batazos* ** (`Hits`) de la temporada anterior y del número de **años** (`Years`) que lleva jugando en la MLB 


---
# Árboles de regresión

```{r, fig.align='center', fig.height=4,  fig.width=6, dev='svg'}
library(ISLR)
data("Hitters")
pairs(~ Salary + Years + Hits, data=Hitters)
```

---
# Árboles de regresión

```{r, fig.align='center', fig.height=4,  fig.width=6, dev='svg', warning=FALSE}
library(tree)
tree.model <- tree(Salary ~ Years + Hits, data = Hitters,
                   control=tree.control(nobs=322, 
                                        minsize =100))
plot(tree.model)
text(tree.model, all=TRUE)
```


---
# Árboles de regresión

```{r, echo=FALSE, fig.align='center', fig.height=5,  fig.width=7, dev='svg'}
plot(Hitters$Years, Hitters$Hits, xlab="Years", ylab="Hits", bg="orange", pch = 21)
lines(x = c(4.5, 4.5), y = c(-20,350))
text(x = 2.5, y = 200, labels = c("R1"), cex=2)
lines(x = c(4.5,40), y = c(117.5, 117.5))
text(x = 17, y = 30, labels = c("R2"), cex=2)
text(x = 17, y = 190, labels = c("R3"), cex=2)

```

---
# Árboles de regresión

El *truco* de lo árboles de decisión es la **división binaria recursiva** (*recursive binary splitting*). En el caso de la regresión, consiste en lo siguiente:

+ En el primer paso, se selecciona el predictor $X_j$ y el punto de corte $s$ que divide el espacio en dos regiones $R_1$ y $R_2$ **que minimizan la siguiente ecuación**:

$\Large \sum_{i: x_i \in R_1(j,s)}(y_i-\hat{y}_{R_1})^2+\sum_{i: x_i \in R_2(j,s)}(y_i-\hat{y}_{R_2})^2$

+ Es decir, consideramos **todos los predictores** $X_i$ y **todos los posibles puntos de corte** y luego elegimos el predictor y el punto de corte que hace que el árbol resultante **tenga el RSS mínimo**.

+ En cada una de las siguientes regiones **se vuelve a repetir el paso anterior**; el proceso continúa hasta que se alcanza un **criterio de detención**

  + p.e.: que ninguna región contenga más de cinco observaciones

---
# Árboles de regresión

Los árboles de decisión **también se podan** para evitar el sobreajuste (es decir, buen ajuste en el conjunto de entrenamento pero malo en el de test)

Esto ocurre cuando los árboles que s generan son **muy complejos** (*grandes*)

Un árbol mas pequeño, con menos divisiones, puede presentar menos varianza (más estabilida) con un pequeño coste de error *bias* y además ser más fácil de interpretar

Hay varias estrategias de poda:

+ **Reducción mínima de RSS**
  + Si no se reduce el RSS por encima de cierto umbral (alto), el proceso se detiene. Este método es demasiado *miope* dado que se pueden perder buenos splits en nodos particulares

+ **Tamaño mínimo de los nodos finales**
  + Si las divisiones generan nodos *pequeños* (típicamente en términos de % de observaciones de la población inicial) el proceso se detiene
  
---

# Árboles de regresión

+ **Poda del enlace más débil (*weakest link pruning*)**

  + Para un árbol grande, evaluar todos los posibles *subárboles* puede ser muy complejo dado el enorme número de posibilidades. 
  
  + El método de *weakest link pruning* genera *subárboles* que dependen de un parámetro $\alpha$ sobre los que ejecutar una validación cruzada
  
  + La forma de hacerlo recuerda al método LASSO de regularización ya que dado un valor de $\alpha$, el *subárbol* $T$ que se genera es aquel que minimiza la siguiente ecuación (con $T$ el número nodos terminales y $R_m$ el rectángulo n-ésimo):
  
  $\LARGE \sum_{m=1}^{\left |T \right |}\sum_{i:x_i \in R_m}(y_i-\hat y_{R_m})^2+\alpha \left | T \right |$
  
  + El valor óptimo de $\alpha$ puede obtenerse mediante *K-fold cross-validation* y por tanto, el subárbol óptimo también (el asociado a dicho $\alpha$)

---

# Árboles de clasificación

Son similares a los árboles de regresión pero **ajustan una respuesta cualitativa**

Ahora la predicción, en vez de ser la la respuesta media del nodo al que pertenece la observación, es la clase mayoritaria

Para hacer crecer el árbol se utilizan varias **medidas alterinativas al RSS** (que ahora carece de sentido):

+ **Tasa de error de clasificación (*classification error rate*)**: es el % de casos mal clasificados
  
  $\Large E=1-max_k(\hat p_{mk})$
  
  en donde $p_{mk}$ representa la proporción de datos mal clasificados en la m-ésima región (nodo terminal) que pertenecen a la k-ésima clase. Esta medida es muy poco sensible en la práctica.
  
  
---
# Árboles de clasificación

+ **Índice de Gini**: es una medida de la varianza total a través de las $K$ clases
  
  $\Large G=\sum_{k=1}^K \hat p_{mk}(1-\hat p_{mk})$

  Cuando los valores de $\hat p_{mk}$ son muy cercanos a 0 o a 1, el índice de Gini es muy bajo

+ **Entropía**: está basada en la teoría de la información de Shannon y también mide lo *puros* que son los nodos
  
  $\Large D=-\sum_{k=1}^K \hat p_{mk} log\hat p_{mk}$
  
  Dado que $0 \leq  \hat p_{mk} \leq  1$ entonces se tiene que $0 \leq -\hat p_{mk}log\hat p_{mk}$ y también se demuestra entonces que cuando los valores de $\hat p_{mk}$ son muy cercanos a 0 o a 1, la entropía es también muy baja

---
# Pros y contras de los árboles de decisión

.pull-left[
###Pros

+ Son muy **fáciles de explicar la gente**, más incluso que una regresión lineal

+ Tienen similitudes **con la forma en la que pensamos** y decidimos

+ Se pueden **graficar e interpretar fácilmente** aunque no se sea experto en la materia

+ Manejan variable cualitativas **sin necesidad de generar variables *dummy* **


]

.pull-right[
###Contras

+ Generalmente, **no son tan buenos prediciendo como otros métodos**

+ Pueden ser **poco robustos**: pequeños cambios en los datos pueden hacer variar notablemente el árbol resultante (sufren de *alta varianza*)


]


---
# Árboles de clasificación

Vamos a hacer un árbol de clasificación sobre el dataset `Carseats`, que son datos de ventas de asientos de niño para coche en 400 tiendas con 11 variables:
+ `Sales`: Unidades vendidas en cada tienda (miles)
+ `CompPrice`: Precio de la competenci en cada tienda
+ `Income`: Nivel del ingresos de la zona (miles de dólares)
+ `Advertising`: Presupuesto para publicidad local en la zona (miles de dólares)
+ `Population`: Población en la región (miles de habitantes)
+ `Price`: Precio del asiento en la tienda
+ `ShelveLoc`: Calidad de localización en la estantería del asiento (Bad, Good and Medium) 
+ `Age`: Edad media de la población de la zona
+ `Education`: Nivel medio de estudios de la zona
+ `Urban`: Indica si la tienda está ubicada en zona urbana o rural
+ `US`: Indica si la tienda está ubicada en EEUU o no

---
# Árboles de clasificación

Cargamos los datos y creamos una variable llamada `High` que toma valor `Yes` si las ventas (`Sales`) superan 8 y `No` en otro caso:

```{r, warning=FALSE, message=FALSE}
library(tree)
library(ISLR)
attach(Carseats)
High=ifelse(Sales<=8, "No", "Yes")

```

Uusamos la función `data.frame()` para *pegar* la variable al dataset original:

```{r, warning=FALSE, message=FALSE}
Carseats=data.frame(Carseats, High)

```

Ajustamos un árbol para predecir `High` usando el resto de variables (excepto `Sales`):

```{r, warning=FALSE, message=FALSE}
tree.carseats=tree(High~.-Sales, Carseats)

```


---
# Árboles de clasificación

Vemos los estadísticos básicos resultantes:

```{r, warning=FALSE, message=FALSE}
summary(tree.carseats)

```

El error de entrenamiento es del 9%

---
# Árboles de clasificación

Dibujamos el árbol con `plot()` y `text()`:

```{r, warning=FALSE, message=FALSE, fig.align='center', fig.height=5,  fig.width=7, dev='svg'}
plot(tree.carseats)
text(tree.carseats, pretty=0)

```

---
# Árboles de clasificación

Para ver el detalle de los nodos por pantalla hay que llamar al objeto árbol creado, en nuestro caso `tree.carseats`<sup>(1)</sup>:

```{r, eval=FALSE}
tree.carseats
```

Para evaluar la precisión del modelo vamos a dividir el dataset en training y test, construir el árbol con éste último conjunto y hace rpredicción sobre el primero. Empezamos dividiendo el *dataset* original:

```{r, message=FALSE, warning=FALSE}
set.seed(2)
train=sample(1:nrow(Carseats), 200)
Carseats.test=Carseats[-train,]
High.test=High[-train]
```


.footnote[(1) ¡No cabe en la pantalla!]

---
# Árboles de clasificación

Ajustamos el árbol sobre el entrenamiento y hacemos predicción sobre test; sacamos la matriz de confusión:

```{r, message=FALSE, warning=FALSE}
tree.carseats=tree(High~.-Sales, Carseats, subset=train)
tree.pred=predict(tree.carseats, Carseats.test, type="class")
table(tree.pred, High.test)
```

La precisión del modelo es de `(86+57)/200=0.715`

---
# Árboles de clasificación

Ahora vamos a podar el árbol para mejorar los resultados, si es posible. La función `cv.tree()` realiza una validación cruzada para determinar la complejidad óptima del árbol mediante la poda del enlace más débil (también llamada *cost complexity pruning*). Usamos el argumento `FUN=prune.misclass` para indicar que queremos utilizar el *missclasification rate* para guiar el proceso de poda pues por defecto utiliza un estadístico llamado *deviance*.

La salida de la función `cv.tree()` muestra el número de nodos terminales de cada árbol resultante así como su correspondiente tasa de error y el valor del parámetro de *cost-complexity* asociado (el parámetro $\alpha$) que vimos anteriormente

```{r, warning=FALSE, message=FALSE}
set.seed(3)
cv.carseats=cv.tree(tree.carseats, FUN=prune.misclass)

```

---
# Árboles de clasificación

```{r, warning=FALSE, message=FALSE}
cv.carseats

```

---
# Árboles de clasificación

La variable `dev` es el nº de valores mal clasificados así que el árbol *óptimo* es el de tamaño 9. Podemos dibujar la tasa de error en función de `size`:

```{r, warning=FALSE, message=FALSE, fig.align='center', fig.height=4,  fig.width=7, dev='svg'}
plot(cv.carseats$size, cv.carseats$dev, type="b")
```

---
# Árboles de clasificación

Aplicamos `prune.misclass` para podar el árbol y vemos el resultado:

```{r, warning=FALSE, message=FALSE, fig.align='center', fig.height=4,  fig.width=7, dev='svg'}
prune.carseats=prune.misclass(tree.carseats, best=9)
plot(prune.carseats)
text(prune.carseats, pretty=0)
```

---
# Árboles de clasificación

```{r, message=FALSE, warning=FALSE}
tree.pred=predict(prune.carseats, Carseats.test, type="class")
table(tree.pred, High.test)
```

El nuevo árbol es **mas robusto**, **mas fácil de interpretar** y **mejora la tasa de error del anterior**.

---
# Modelos basados en árboles: Bagging

+ El *bootstraping* es una técnica muy poderosa que se puede emplear también **para mejorar modelos de *machine learning* ** como los árboles de decisión

+ En general, los árboles de decisión **sufren de alta varianza** (inestabilidad frente a distintas muestras de entrenamiento)

+ El *bagging* (también llamado *bootstrap aggregation*) es un procedimiento destinado a reducir la varianza de un modelo de machine learning, particularmente útil en el caso de los árboles de decisión.

+ Todo se basa en el siguiente hecho:
  + Dada una colección de $n$ observaciones independientes $Z_1, Z_2, ...Z_n$, cada una de ellas con varianza $\sigma^2$, la varianza de la media $\bar Z$ de las observaciones es igual a $\sigma^2/n$
  + Es decir: **la media reduce la varianza**

---

# Modelos basados en árboles: Bagging

+ Así pues una manera natural de reducir la varianza e incrementar la precisión de las predicciones de un modelo de machine learning es **extraer ditintas muestras del  conjunto de entrenamiento, ajustar un modelo para cada una de ellas, y hacer la media de las predicciones**.

+ Así pues podemos calcular $\hat f^{*1}(x), \hat f^{*2}(x), ..., \hat f^{*B}(x)$ usando $B$ **muestras distintas del conjunto de entrenamiento** y *hacer la media de ellos* para *construir* un modelo de menor varianza; el modelo final sería:

$\large \hat f_{bag}(x)=1/B \sum_{b=1}^{B}\hat f^{*b}(x)$

+ Cada uno de los árboles individuales se construyen con profundidad y no se podan, de manera que todos ellos tienen **mucha varianza** y **poco bias**

+ El bagging ha demostrado **muy buenos resultados** combinando muchos árboles (cientos e incluso miles) 

+ En el caso de la clasificación, la manera típica de combinar los árboles es mediante **voto mayoritario**; es decir: una nueva observación se clasifica en la clase en la que la mayoría de árboles individuales la clasifican.

---

# Modelos basados en árboles: Bagging

+ Otra ventaja de los modelos de bagging es que **automáticamente generan una medida del error de estimación**.

+ Como cada árbol individual utiliza una parte de los datos de entrenamiento originales (típicamente dos tercios de las observaciones), **se puede utilizar el resto de observaciones llamadas *out-of-bag* (OOB) para medir el error**
  + Intuitivamente, si utilizamos $2/3$ de las observaciones para entrenar cada árbol individual, entonces se tienen unas $B/3$ predicciones *out-of-bag* distintas para cada observación; el promedio (o el voto mayoritario) es una previsión agregada que sirve para medir el error de estimación del modelo.  

+ Los modelos de bagging aportan además información sobre la **importancia de las variables**, promediando el decremento de RSS (en el caso de regresión) o del índice de Gini (en el de clasificación)  

---
# Modelos basados en árboles: Random Forest

+ Pueden suponer una mejora de los modelos de bagging, mediante un sencillo truco que  *descorrela* los árboles individuales

+ Al igual que en el bagging, se construyen unos cuantos árboles sobre muestras distintas del conjunto de entrenamiento, **pero ahora también se muestrean las variables**: cada árbol se constuye sobre una muestra distinta de observaciones y de predictores

+ Típicamente se utiliza un número $m$ de variables $m \approx \sqrt p$ donde $p$ es el número total de variables

+ *No dejar que cada arbol utilice todas las variables* parece una **barbaridad** pero **puede resultar bueno**, sobre todo si existe un predictor muy fuerte
  + Trabajando con todas las variables estaríamos haciendo siempre árboles muy parecidos (correlados) y el árbol promedio no reducirá mucho la varianza
  + Trabajando sólo con una parte de los predictores, el resto de variables toma más protagonismo y genera árboles incorrelados, cuyo promedio es más robusto
  
---
# Modelos basados en árboles: ejemplo Random Forest y Bagging

Tanto los modelos de Boosting como los de Random Forest se pueden ajustar con la función `randomForest` de la librería del mismo nombre.

Para el bagging basta con muestrear un cantidad de variables igual al de predictores. Eso se hace con el parámetro `mtry` (en nuestro caso tenemos 10 predictores):


```{r, message=FALSE, warning=FALSE}
library(randomForest)
train=sample(1:nrow(Carseats), nrow(Carseats)/2)
bag.Carseats=randomForest(High~.-Sales, data=Carseats, 
                          subset=train, mtry=10)

```

.footnote[Nota: Hemos seleccionado la mitad de los registros para el conjunto de entrenamiento y la otra mitad para el de test]


---
# Modelos basados en árboles: ejemplo Random Forest y Bagging

```{r, message=FALSE, warning=FALSE}
bag.Carseats

```

---
# Modelos basados en árboles: ejemplo Random Forest y Bagging

¿Cómo lo hace el bagging sobre el conjnto de test?

```{r, message=FALSE, warning=FALSE}
yhat.bag = predict(bag.Carseats, newdata = Carseats[-train,])
Carseats.test=Carseats[-train,"High"]
table(yhat.bag, Carseats.test)

```

La precisión del modelo es de `r (108+54)/200`, mejor que el `0.715` que obtuvimos antes

---
# Modelos basados en árboles: ejemplo Random Forest y Bagging

Ahora estimamos un random forest con 3 variables:

```{r, message=FALSE, warning=FALSE}
rf.Carseats=randomForest(High~.-Sales, 
                         data=Carseats, 
                         subset=train, 
                         mtry=3)
```

---
# Modelos basados en árboles: ejemplo Random Forest y Bagging

```{r, message=FALSE, warning=FALSE}
rf.Carseats
```


---
# Modelos basados en árboles: ejemplo Random Forest y Bagging

```{r}
yhat.rf = predict(rf.Carseats, newdata = Carseats[-train,])
table(yhat.rf, Carseats.test)
```

La precisión del modelo es de `r (107+51)/200`, mejor que el `0.715` que obtuvimos antes pero peor que la de boosting

---
# Modelos basados en árboles: Boosting

+ El boosting es otro enfoque **para mejorar la precisión de un árbol de decisión** (y en general de muchos otros métodos de machine learning)

+ También implica la creación de varios árboles, solo que **esta vez se crean secuencialmente**

 + Cada árbol se crea **usando información de los árboles anteriores**

+ Otra diferencia importante es que el boosting **no implica bootstraping sobre el conjunto de entrenamiento**

+ Los modelos de boosting va reduciendo el error *poco a poco*: **cada árbol utiliza como variable independiente el error del árbol anterior** y todos ellos se van agregando a la función de ajuste para reajustar el error.

+ Un parámetro ajusta el tamaño de los árboles, que **típicamente son pequeños**

---
# Modelos basados en árboles: Boosting

Los modelos de boosting tienen **tres parámetros** de *tunning*:

+ El número $B$ de árboles, que se suele ajustar mediante cross-validation para no caer n sobreajuste

+ El parámetro $\lambda$ de encogimiento (*shrinkage*), un número positivo pequeño. Controla la velocidad con la que el modelo aprende. Los valores típicos están entre 0.01 y 0.001. Un valor muy pequeño hará que neesitemos un valor muy alto de B para lograr una precisión alta

+ El número $d$ de splits de cada árbol, que controla la complejidad del modelo. A veces $d=1$ funciona bien

---
# Ejemplo: Boosting

Para hacer un modelo de boosting utilizamos la librería `gbm`. Hay que transformar la $Y$ en una variable `0-1` y decirle que es un modelo de clasificación mediante `distribution="bernoulli"`

```{r, warning=FALSE, message=FALSE}
library(gbm)
Carseats$High=ifelse(Carseats$High=="No", 0, 1)
boost.Carseats=gbm(High~.-Sales, 
                   data=Carseats[train,], 
                   distribution="bernoulli",
                   n.trees = 5000,
                   interaction.depth = 4)

```

---
# Ejemplo: Boosting

```{r, warning=FALSE, message=FALSE}
yhat.boost = predict(boost.Carseats, 
                     newdata = Carseats[-train,], 
                     n.trees = 5000)

table(ifelse(yhat.boost<0.5, 0, 1), Carseats.test)

```

En este caso, la precisión del modelo es `r (116+44)/200`, similar a la del random forest y a la del boosting

---
# Taller: Random forest

Vamos a hacer un modelo de Random Forest sobre [el dataset HR-Employee-Attrition](https://www.ibm.com/communities/analytics/watson-analytics-blog/hr-employee-attrition/) de IBM.

+ **Objetivo**: predecir cuándo un empleado está a punto de *irse de la empresa*
+ **Qué vamos a hacer**:
  + Cargar el dataset
  + Análisis exploratorio
  + Una validación cruzada para elegir el número óptimo de árboles a entrenar por el random forest
  + Predicción sobre el modelo óptimo
  
---
# Taller: Random forest

Podemos cargar el dataset directamente de una url y leerlo en R. Una opción es la siguiente:

```{r, warning=FALSE, message=FALSE}

library(dplyr)
library(ggplot2)
library(readxl)

datos <- read_excel("data/WA_Fn-UseC_-HR-Employee-Attrition.xlsx")

```

---
# Taller: Random forest

Lo primero es lo primero:

```{r, eval=FALSE}
str(datos)
```

Hacemos un summary

```{r, eval=FALSE}
summary(datos)
```

Puedes comprobar el metadata del excel para entender las variables

---
# Taller: Random forest

Vamos a quitar algunas variables que no son relevantes:
+ `EmployeeCount`
+ `EmployeeNumber`
+ `Over18`

```{r, echo=TRUE, warning=FALSE, message=FALSE}
quitar <-  c("EmployeeCount", "EmployeeNumber", "Over18")
datos %>% select(-quitar) -> datos

```

---
# Taller: Random forest

El análisis exploratorio es muy útil para **entender** los datos, sacar unos **primeros resultados**, tener una idea de si el modelo puede tener éxito o no y anticipar cuáles van a ser las variables significativas. Algunos gráficos:

```{r, fig.align='center', fig.height=3,  fig.width=7, dev='svg'}
ggplot(datos, aes(Age,fill=Attrition))+geom_density()+facet_grid(~Attrition)
```


---
# Taller: Random forest

```{r, fig.align='center', fig.height=3,  fig.width=7, dev='svg'}
ggplot(datos, aes(BusinessTravel, fill=Attrition)) + geom_bar()
```

---
# Taller: Random forest
Para que se vean mejor los gráficos, los ponemos en base 100:

```{r, warning=FALSE, message=FALSE, fig.align='center', fig.height=3,  fig.width=7, dev='svg'}
datos2 <- datos %>% group_by(BusinessTravel,Attrition) %>% 
  summarise(count=n()) %>% mutate(perc=count/sum(count))

ggplot(datos2, aes(x = BusinessTravel, y = perc*100, fill = Attrition)) +
  geom_bar(stat="identity") +
  labs(x = "BusinessTravel", y = "percent", fill = "Attrition")
```

---

# Taller: Random forest

Hacemos un text *chi-cuadrado* con las variables categóricas y el target (para las cuantitativas existe un test equivalente, llamado ANOVA):

```{r, warning=FALSE, message=FALSE}
factores <- setdiff(names(Filter(is.character, datos)), c("Attrition"))

lapply(factores, function(x) 
  data.frame(variable=x,
             p.value=chisq.test(table(datos[,which(names(datos) %in% c("Attrition", x))]))$p.value)) %>%   bind_rows()
```

¿Cuál es la relación más significativa? ¿Y la menos? Pintarlas


---
# Taller: Random forest

Lo primero que hacemos es guardarnos unos cuantos datos de test para hacer previsión
```{r}
library(caret)
set.seed(12345)
inTrain <- createDataPartition(datos$Attrition,p=0.75,list = FALSE)
datos_train <- datos[inTrain,]
datos_test  <- datos[-inTrain,]

```

---
# Taller: Random forest

Caret te permite hacer fácilmente dos cosas importantes: rastrear parámetros en un grid y hacer cross-validation para obtener un modelo óptimo en base a una buena estimación de su error

```{r, eval=FALSE}

control <- trainControl(method="repeatedcv", 
                        number=10, repeats=3, 
                        search="grid")
tunegrid <- expand.grid(.mtry=c(6:10))

rf_grid <- train(Attrition ~ . , 
                   data=datos_train, 
                   method="rf", 
                   metric="Accuracy", 
                   tuneGrid=tunegrid,
                   trControl=control)
```

---
# Taller: Random forest

```{r, echo=FALSE}
rf_grid=readRDS("data/rf_grid.RDS")
```

```{r, eval=FALSE}
print(rf_grid)
```

---
# Taller: Random forest

```{r, warning=FALSE, message=FALSE, echo=FALSE}
print(rf_grid)
```

---

# Taller: Random forest


```{r, fig.align='center', fig.height=3,  fig.width=7, dev='svg'}
plot(rf_grid)
```

---
# Taller: Random forest

Hacemos las predicciones y vemos la matriz de confusión:

```{r, eval=FALSE}
predicciones <- predict(rf_grid, newdata = datos_test, type = "raw")
confusionMatrix(predicciones, datos_test$Attrition)

```

Podemos ver también la importancia de las variables

```{r, eval=FALSE}
varImp(rf_grid)
```


---

# Taller: Random forest

```{r, fig.align='center', fig.height=3,  fig.width=7, dev='svg'}
ggplot(datos, aes(MonthlyIncome,fill=Attrition))+geom_density()+facet_grid(~Attrition)
```

---

# Taller: Random forest

```{r, fig.align='center', fig.height=3,  fig.width=7, dev='svg'}
ggplot(datos, aes(TotalWorkingYears,fill=Attrition))+geom_density()+facet_grid(~Attrition)
```



---

class: inverse, center, middle

# Práctica para casa: árboles

---
#  Práctica para casa: árboles

Hacer un árbol de clasificación sobre el dataset `OJ` del paquete `ISLR` (más info [aquí](https://vincentarelbundock.github.io/Rdatasets/doc/ISLR/OJ.html))

1. Crea un conjunto de entrenamiento con 800 observaciones y otro de test con el resto
1. Ajusta un árbol sobre el conjunto de entrenamiento, con `Purchase` como respuesta y el resto de variables como predictores. Describe los estadísticos del `summary()`: ¿cuál es la tasa de error? ¿cuántos nodos terminales tiene?
1. Haz un gráfico del árbol e interperta los resultados
1. Haz una predicción de los datos del conjunto de test y haz una matriz de confusión: ¿cuál es la tasa de error?
1. Ejecuta `cv.tree()` para calcular el tamaño óptimo del árbol
1. Haz un gráfico con el tamaño del árbol en el eje x y el error calculado mediante cross-validation en el eje y: ¿cuál es el tamaño que corresponde al mínimo error?
1. Genera un árbol podado que se corresponda con ese tamaño óptimo
1. Compara los errores de entrenamiento del árbol podado y del original: ¿cuál es menor?
1. Compara los errores de test del árbol podado y del original: ¿cuál es menor?


